#define _GNU_SOURCE
#include <linux/unistd.h>
#include <linux/syscalls.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#define sys_eabi_print() __asm__ __volatile__ ("mov x8,#294\n\t" "mov x0,#0\n\t"  "svc 0\n\t" )

static int test_init(void) {
  printk("test module init");
  //  sys_eabi_print();
    return 0;
}

static void test_exit(void) {
  printk("test module exit");
}


module_init(test_init);
module_exit(test_exit);

MODULE_DESCRIPTION("the module of testing sys_print syscall");
MODULE_LICENSE("GPL");
